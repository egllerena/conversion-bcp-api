insert into tc_usuario (username, password) 
values 
('administrador','$2a$10$Qh/C5BFSC6NGPrZhOqQQFej0WUTbT5zfc0PCOhLSkGng/EswyDl2C'),
('user','$2a$10$Qh/C5BFSC6NGPrZhOqQQFej0WUTbT5zfc0PCOhLSkGng/EswyDl2C');

insert into tc_authorities (id_user, authority) 
values(1, 'ROLE_USER'),(1,'ROLE_ADMIN'),(2,'ROLE_USER');

insert into tc_moneda (x_nombre_moneda, x_simbolo_moneda, x_nombre_jpg) 
values 
('nuevo sol','PEN','bandera-del-peru-300x200.jpg'),
('afgani afgano','AFN','bandera-de-afganistan-300x200.jpg'),
('libra esterlina','GBP','bandera-reino-unido-300x150.jpg'),
('euro','EUR','bandera-de-la-union-europea-300x200.jpg'),
('peso argentino','ARS','bandera-argentina-300x188.jpg'),
('dólar australiano','AUD','bandera-de-australia-300x150.jpg'),
('boliviano','BOB','bandera-de-bolivia-300x205.jpg'),
('real brasileño','BRL','bandera-brasil-300x200.jpg'),
('leva búlgaro','BGN','bugaria-bandera-300x180.jpg'),
('dólar canadiense','CAD','bandera-de-canada-300x150.jpg'),
('peso chileno','CLP','bandera-chile-300x200.jpg'),
('yuan','yan','bandera-de-china-300x200.jpg'),
('peso colombiano','COP','bandera-colombia-300x200.jpg'),
('colón costarricense','CRC','bandera-de-costa-rica-300x180.jpg'),
('kuna croata','HRK','bandera-croacia-300x150.jpg'),
('corona danesa','DKK','bandera-dinamarca-300x227.jpg'),
('peso dominicano','DOP','bandera-de-republica-dominicana-300x200.jpg'),
('dólar estadounidense','USD','bandera-estados-unidos-300x158.jpg'),
('libra egipcia','EGP','bandera-de-egipto-300x200.jpg'),
('bir','ETB','bandera-de-etiopia-300x150.jpg'),
('quetzal','GTQ','bandera-de-guatemala-300x200.jpg'),
('gourde','HTG','bandera-de-haiti-300x180.jpg'),
('rial iraní','IRR','bandera-de-iran-300x171.jpg'),
('dinar iraquí','IQD','bandera-de-irak-300x200.jpg'),
('nuevo séquel','ILS','bandera-de-israel-300x218.jpg'),
('yen japonés','JPY','bandera-de-japon-300x200.jpg'),
('won surcoreano','KRW','bandera-de-corea-del-sur-300x200.jpg'),
('dólar liberiano','LRD','bandera-de-liberia-300x158.jpg'),
('ariary malgache','MGA','bandera-de-madagascar-768x512.jpg'),
('uguiya','MRO','bandera-de-mauritania-300x200.jpg'),
('peso mexicano','MXN','bandera-mexico-300x171.jpg'),
('dírham marroquí','MAD','bandera-de-marruecos-300x200.jpg'),
('metical','MZN','mozambique-bandera-300x200.jpg'),
('dólar neozelandés','NZD','bandera-de-nueva-zelanda-1-300x150.jpg'),
('corona noruega','NOK','bandera-noruega-300x218.jpg'),
('balboa panameño','PAB','bandera-de-panama-300x200.jpg'),
('peso filipino','PHP','bandera-de-filipinas-300x150.jpg'),
('leu rumano','RON','bandera-rumania-300x200.jpg'),
('rublo','RUB','bandera-rusia-300x200.jpg'),
('rand sudafricano','ZAR','bandera-de-sudafrica-300x200.jpg'),
('rupia de Sri Lanka','LKR','bandera-sri-lanka-300x150.jpg'),
('lilangeni','SZL','bandera-de-suazilandia-300x200.jpg'),
('franco suizo','CHF','Bandera-Suiza-300x300.jpg'),
('somoni tayiko','TJS','bandera-de-tajikistan-300x150.jpg'),
('dinar tunecino','TND','bandera-de-tunez-300x200.jpg'),
('bolívar venezolano','VEF','bandera-de-venzuela-300x200.jpg'),
('rial yemení','YER','bandera-de-yemen-300x200.jpg');


insert into tc_historial_cambio (id_moneda, n_precio_compra, n_precio_venta, f_fecha_cambio)
values (1,1,1,current_timestamp),
(2,0.0466,0.047,current_timestamp),(3,4.5454,4.63,current_timestamp),(4,4.1666,4.22,current_timestamp),(5,0.0464,0.046,current_timestamp),(6,2.56,2.56,current_timestamp),(7,0.518,0.52,current_timestamp),
(8,0.636,0.64,current_timestamp),(9,2.173,2.15,current_timestamp),(10,2.702,2.703,current_timestamp),(11,0.0045,0.0045,current_timestamp),(12,0.531,0.532,current_timestamp),(13,0.0009,0.00093,current_timestamp),
(14,0.0060,0.0064,current_timestamp),(15,0.558,0.56,current_timestamp),(16,0.564,0.57,current_timestamp),(17,0.061,0.061,current_timestamp),(18,3.571,3.58,current_timestamp),(19,0.227,0.23,current_timestamp),
(20,0.097,0.097,current_timestamp),(21,0.460,0.46,current_timestamp),(22,0.056,0.057,current_timestamp),(23,0.000084,0.000085,current_timestamp),(24,0.0030,0.0030,current_timestamp),
(25,1.052,1.053,current_timestamp),(26,0.033,0.034,current_timestamp),(27,0.0031,0.0031,current_timestamp),(28,0.018,0.018,current_timestamp),(29,0.00091,0.00092,current_timestamp),
(30,0.092,0.093,current_timestamp),(31,0.166,0.17,current_timestamp),(32,0.38,0.39,current_timestamp),(33,0.048,0.049,current_timestamp),(34,2.38,2.38,current_timestamp),
(35,0.38,0.39,current_timestamp),(36,3.57,3.58,current_timestamp),(37,0.073,0.074,current_timestamp),(38,0.862,0.87,current_timestamp),(39,0.045,0.046,current_timestamp),
(40,0.21,0.22,current_timestamp),(41,0.018,0.019,current_timestamp),(42,0.21,0.22,current_timestamp),(43,3.90,3.91,current_timestamp),(44,0.34,0.35,current_timestamp),
(45,1.29,1.30,current_timestamp),(46,0.0000081,0.0000082,current_timestamp),(47,0.0000081,0.0000082,current_timestamp);




