package pe.com.example.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RespuestaBase<T> {
    private ErrorCode errorCode;
    private T data;

    public static RespuestaBase successNoData() {
        return RespuestaBase.builder()
                .build();
    }

    public static <T> RespuestaBase<T> successWithData(T data) {
        return RespuestaBase.<T>builder()
                .data(data)
                .build();
    }

    public static RespuestaBase error(ErrorCode errorCode) {
        return RespuestaBase.builder()
                .errorCode(errorCode)
                .build();
    }
}