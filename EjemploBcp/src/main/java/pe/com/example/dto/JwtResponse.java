package pe.com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.com.example.entity.UsuarioEntity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JwtResponse {
	private String mensaje;
	private String token;
	private int status;
	private UsuarioEntity user;
}
