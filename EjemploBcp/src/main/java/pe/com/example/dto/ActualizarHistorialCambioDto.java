package pe.com.example.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ActualizarHistorialCambioDto {
	private String nombreMoneda;
	private BigDecimal precioCompra;
	private BigDecimal precioVenta;
}
