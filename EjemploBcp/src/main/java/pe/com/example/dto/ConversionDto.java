package pe.com.example.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ConversionDto {
	private BigDecimal monto;
	private BigDecimal montoConvertido;
	private Long monedaOrigen;
	private Long monedaDestino;
	private String tipoCambio;
}
