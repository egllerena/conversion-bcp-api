package pe.com.example.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pe.com.example.entity.MonedaEntity;

@Repository
public interface MonedaRepository extends JpaRepository<MonedaEntity, Long>{
	
	@Query("select m from MonedaEntity m where LOWER(m.nombreMoneda) = LOWER(:nombreMoneda) ")
	Optional<MonedaEntity> buscarPorNombreMoneda(@Param("nombreMoneda")String nombreMoneda);	
}
