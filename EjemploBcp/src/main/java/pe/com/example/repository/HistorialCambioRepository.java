package pe.com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.example.entity.HistorialCambioEntity;
import pe.com.example.entity.MonedaEntity;

@Repository
public interface HistorialCambioRepository extends JpaRepository<HistorialCambioEntity, Long>{
	List<HistorialCambioEntity> findByIdMoneda(MonedaEntity idMoneda);
}
