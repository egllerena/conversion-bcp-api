package pe.com.example.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="tc_historial_cambio")
public class HistorialCambioEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idHistorialCambio;
	
	@JoinColumn(name="id_moneda")
	@ManyToOne(optional=true, fetch=FetchType.EAGER)
	private MonedaEntity idMoneda;
	
	@Column(name="n_precio_compra",precision=9, scale = 4)
	private BigDecimal precioCompra;
	
	@Column(name="n_precio_venta",precision=9, scale = 4)
	private BigDecimal precioVenta;
	
	@Column(name="f_fecha_cambio")	
	private Date fechaCambio;
}
