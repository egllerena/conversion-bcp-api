package pe.com.example.service;

import java.util.List;

import pe.com.example.entity.UsuarioEntity;

public interface UsuarioService {
	public List<UsuarioEntity> obtenerTodosUsuarios();
}
