package pe.com.example.service;

import java.util.List;

import pe.com.example.dto.ActualizarHistorialCambioDto;
import pe.com.example.dto.ConversionDto;
import pe.com.example.dto.ConversionStringDto;
import pe.com.example.dto.ConvertirDto;
import pe.com.example.dto.ConvertirStringDto;
import pe.com.example.dto.HistorialCambioFiltro;
import pe.com.example.entity.HistorialCambioEntity;
import pe.com.example.entity.MonedaEntity;
import rx.Completable;
import rx.Single;

public interface HistorialCambioService {
	Single<ConversionDto> convertirMonto(ConvertirDto convertirDto);
	Single<ConversionStringDto> convertirMontoStringMoneda(ConvertirStringDto convertirDto);
	Completable actualizarHistorial(HistorialCambioEntity historialCambioEntity);
	Completable actualizarHistorialCambioPorNombreMoneda(ActualizarHistorialCambioDto actualizarHistorialCambioDto);
	Single<List<HistorialCambioEntity>> obtenerHistoriales(HistorialCambioFiltro historialCambioFiltro);
	Single<HistorialCambioEntity> obtenerHistorial(Long idHistorial);
	Single<List<HistorialCambioEntity>> obtenerDetalleConversionWeb(List<MonedaEntity> listamonedas);
}
