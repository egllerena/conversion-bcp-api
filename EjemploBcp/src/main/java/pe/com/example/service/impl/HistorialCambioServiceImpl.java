package pe.com.example.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.example.dto.ActualizarHistorialCambioDto;
import pe.com.example.dto.ConversionDto;
import pe.com.example.dto.ConversionStringDto;
import pe.com.example.dto.ConvertirDto;
import pe.com.example.dto.ConvertirStringDto;
import pe.com.example.dto.HistorialCambioFiltro;
import pe.com.example.entity.HistorialCambioEntity;
import pe.com.example.entity.MonedaEntity;
import pe.com.example.repository.HistorialCambioRepository;
import pe.com.example.repository.MonedaRepository;
import pe.com.example.service.HistorialCambioService;
import rx.Completable;
import rx.Single;

@Service
public class HistorialCambioServiceImpl implements HistorialCambioService{
	
	private static final Long MonedaPeru = 1L;
	private static final String MonedaPeruString = "nuevo sol";
	private static final String TIPO_DIRECTO = "Tipo Directo";
	private static final String TIPO_INDIRECTO = "Tipo Indirecto";
	private static final String TIPO_CRUZADO = "Tipo Cruzado";
	private static final String NO_HUBO_CAMBIO = "No hubo cambio";
	
	@Autowired
	MonedaRepository monedaRepository;
	
	@Autowired
	HistorialCambioRepository historialCambioRepository;

	@Override
	public Single<ConversionDto> convertirMonto(ConvertirDto convertirDto) {
		
		return Single.create(subs -> {
			Optional<MonedaEntity> monedaDivisa = monedaRepository.findById(MonedaPeru);
			if(!monedaDivisa.isPresent()) {
				subs.onError(new EntityNotFoundException());
			} else {
				ConversionDto conversionFinal = new ConversionDto();
				HistorialCambioEntity monedaBase = obtenerUltimoCambio(convertirDto.getMonedaBase());
				HistorialCambioEntity monedaCotizacion = obtenerUltimoCambio(convertirDto.getMonedaCotizacion());
				
				if(!Objects.equals(monedaBase,null) && !Objects.equals(monedaCotizacion,null)) {
					
					convertirDto.setMontoaConvertir(Objects.equals(convertirDto.getMontoaConvertir(), null)?BigDecimal.ZERO:convertirDto.getMontoaConvertir());
					
					conversionFinal.setMonto(convertirDto.getMontoaConvertir());
					conversionFinal.setMonedaOrigen(convertirDto.getMonedaBase());
					conversionFinal.setMonedaDestino(convertirDto.getMonedaCotizacion());
					BigDecimal montoCalculado = BigDecimal.ZERO;
					
					if(Objects.equals(convertirDto.getMonedaBase(), convertirDto.getMonedaCotizacion())) {
						montoCalculado = convertirDto.getMontoaConvertir();
						conversionFinal.setTipoCambio(NO_HUBO_CAMBIO);
					} else {
						if(Objects.equals(MonedaPeru, convertirDto.getMonedaBase())) {
							montoCalculado = monedaCotizacion.getPrecioCompra().compareTo(BigDecimal.ZERO) != 0 ? convertirDto.getMontoaConvertir().divide(monedaCotizacion.getPrecioCompra(),4,RoundingMode.HALF_UP):BigDecimal.ZERO;
							conversionFinal.setTipoCambio(TIPO_DIRECTO);
						} else if(Objects.equals(MonedaPeru, convertirDto.getMonedaCotizacion())) {
							montoCalculado = convertirDto.getMontoaConvertir().multiply(monedaBase.getPrecioVenta());
							conversionFinal.setTipoCambio(TIPO_INDIRECTO);
						} else {
							BigDecimal montoParcial = convertirDto.getMontoaConvertir().multiply(monedaBase.getPrecioVenta());
							montoCalculado = monedaCotizacion.getPrecioCompra().compareTo(BigDecimal.ZERO) != 0 ? montoParcial.divide(monedaCotizacion.getPrecioCompra(),4,RoundingMode.HALF_UP):BigDecimal.ZERO;
							conversionFinal.setTipoCambio(TIPO_CRUZADO);
						}
					}
					conversionFinal.setMontoConvertido(montoCalculado);
				} else {
					subs.onError(new EntityNotFoundException());
				}
				subs.onSuccess(conversionFinal);
			}
		});		
	}
	
	private HistorialCambioEntity obtenerUltimoCambio (Long idMoneda) {
		Optional<MonedaEntity> optMoneda = monedaRepository.findById(idMoneda);
		if(optMoneda.isPresent()) {
			List<HistorialCambioEntity> listaHistorialMoneda = historialCambioRepository.findByIdMoneda(optMoneda.get());
			if(listaHistorialMoneda.size()>0) {
				HistorialCambioEntity historiaMonedaPeru = listaHistorialMoneda.get(0);
				return historiaMonedaPeru;
			}
		}
		return null;
	}
				
	@Override
	public Single<ConversionStringDto> convertirMontoStringMoneda(ConvertirStringDto convertirDto) {
		return Single.create(subs -> {
			Optional<MonedaEntity> monedaDivisa = monedaRepository.findById(MonedaPeru);
			if(!monedaDivisa.isPresent()) {
				subs.onError(new EntityNotFoundException());
			} else {
				ConversionStringDto conversionFinal = new ConversionStringDto();
				HistorialCambioEntity monedaBase = obtenerUltimoCambioStringMoneda(convertirDto.getMonedaBase());
				HistorialCambioEntity monedaCotizacion = obtenerUltimoCambioStringMoneda(convertirDto.getMonedaCotizacion());
				
				if(!Objects.equals(monedaBase,null) && !Objects.equals(monedaCotizacion,null)) {
					
					convertirDto.setMontoaConvertir(Objects.equals(convertirDto.getMontoaConvertir(), null)?BigDecimal.ZERO:convertirDto.getMontoaConvertir());
					
					conversionFinal.setMonto(convertirDto.getMontoaConvertir());
					conversionFinal.setMonedaOrigen(convertirDto.getMonedaBase());
					conversionFinal.setMonedaDestino(convertirDto.getMonedaCotizacion());
					BigDecimal montoCalculado = BigDecimal.ZERO;
					
					if(Objects.equals(convertirDto.getMonedaBase(), convertirDto.getMonedaCotizacion())) {
						montoCalculado = convertirDto.getMontoaConvertir();
						conversionFinal.setTipoCambio(NO_HUBO_CAMBIO);
					} else {
						if(Objects.equals(MonedaPeruString, convertirDto.getMonedaBase())) {
							montoCalculado = convertirDto.getMontoaConvertir().divide(monedaCotizacion.getPrecioCompra(),4,RoundingMode.HALF_UP);
							conversionFinal.setTipoCambio(TIPO_DIRECTO);
						} else if(Objects.equals(MonedaPeruString, convertirDto.getMonedaCotizacion())) {
							montoCalculado = convertirDto.getMontoaConvertir().multiply(monedaBase.getPrecioVenta());
							conversionFinal.setTipoCambio(TIPO_INDIRECTO);
						} else {
							BigDecimal montoParcial = convertirDto.getMontoaConvertir().multiply(monedaBase.getPrecioVenta());
							montoCalculado = montoParcial.divide(monedaCotizacion.getPrecioCompra(),4,RoundingMode.HALF_UP);
							conversionFinal.setTipoCambio(TIPO_CRUZADO);
						}
					}
					conversionFinal.setMontoConvertido(montoCalculado);
				} else {
					subs.onError(new EntityNotFoundException());
				}
				subs.onSuccess(conversionFinal);
			}
		});	
	}	
	
	private HistorialCambioEntity obtenerUltimoCambioStringMoneda (String nombreMoneda) {
		Optional<MonedaEntity> optMoneda = monedaRepository.buscarPorNombreMoneda(nombreMoneda);
		if(optMoneda.isPresent()) {
			List<HistorialCambioEntity> listaHistorialMoneda = historialCambioRepository.findByIdMoneda(optMoneda.get());
			if(listaHistorialMoneda.size()>0) {
				HistorialCambioEntity historiaMonedaPeru = listaHistorialMoneda.get(0);
				return historiaMonedaPeru;
			}
		}
		return null;
	}
		
	
	@Override
	public Completable actualizarHistorial(HistorialCambioEntity historialCambioEntity) {
		return actualizarHistorialCambioRepositorio(historialCambioEntity);
	}
	
	
	private Completable actualizarHistorialCambioRepositorio(HistorialCambioEntity historialCambioEntity) {
		return Completable.create(compSubs ->{
			if(Objects.equals(historialCambioEntity, null)) {
				compSubs.onError(new EntityNotFoundException());
			} else {
				Optional<HistorialCambioEntity> entidad = historialCambioRepository.findById(historialCambioEntity.getIdHistorialCambio());
				if(!entidad.isPresent()) {
					compSubs.onError(new EntityNotFoundException());
				} else {
					HistorialCambioEntity historialCambio = entidad.get();
					historialCambio.setPrecioCompra(historialCambioEntity.getPrecioCompra());
					historialCambio.setPrecioVenta(historialCambioEntity.getPrecioVenta());
					historialCambioRepository.save(historialCambio);
					compSubs.onCompleted();
				}
			}			
 		});
	}

	@Override
	public Completable actualizarHistorialCambioPorNombreMoneda(
			ActualizarHistorialCambioDto actualizarHistorialCambioDto) {
		HistorialCambioEntity historialMoneda = obtenerUltimoCambioStringMoneda(actualizarHistorialCambioDto.getNombreMoneda());
		if(!Objects.equals(historialMoneda, null)) {
			historialMoneda.setPrecioCompra(actualizarHistorialCambioDto.getPrecioCompra());
			historialMoneda.setPrecioVenta(actualizarHistorialCambioDto.getPrecioVenta());
		}
		return actualizarHistorialCambioRepositorio(historialMoneda);
	}

	@Override
	public Single<List<HistorialCambioEntity>> obtenerHistoriales(HistorialCambioFiltro historialCambioFiltro) {
		return Single.create(subs -> {
			List<HistorialCambioEntity> listaHistorial = new ArrayList<>();
			if(Objects.equals(historialCambioFiltro.getIdMoneda(), null)) {
				listaHistorial = historialCambioRepository.findAll();
				subs.onSuccess(listaHistorial);
			} else {
				Optional<MonedaEntity> moneda = monedaRepository.findById(historialCambioFiltro.getIdMoneda());
				if(!moneda.isPresent()) {
					subs.onError(new EntityNotFoundException());
				} else {
					listaHistorial = historialCambioRepository.findByIdMoneda(moneda.get());
					subs.onSuccess(listaHistorial);
				}
			}
		});
	}

	@Override
	public Single<HistorialCambioEntity> obtenerHistorial(Long idHistorial) {
		return Single.create(subs -> {
			Optional<HistorialCambioEntity> historial = historialCambioRepository.findById(idHistorial);
			if(!historial.isPresent()) {
				subs.onError(new EntityNotFoundException());
			} else {
				subs.onSuccess(historial.get());
			}
		});
	}

	@Override
	public Single<List<HistorialCambioEntity>> obtenerDetalleConversionWeb(List<MonedaEntity> listamonedas) {
		return Single.create(subs -> {
			List<HistorialCambioEntity> listaHistorial = new ArrayList<>();
			for (MonedaEntity moneda : listamonedas) {
				listaHistorial.add(obtenerUltimoCambio(moneda.getIdMoneda()));
			}
			subs.onSuccess(listaHistorial);
		});
	}				
}
