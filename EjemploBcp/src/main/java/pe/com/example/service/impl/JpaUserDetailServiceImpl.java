package pe.com.example.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.com.example.entity.RoleEntity;
import pe.com.example.entity.UsuarioEntity;
import pe.com.example.repository.UsuarioRepository;
import pe.com.example.service.JpaUserDetailService;

@Service
public class JpaUserDetailServiceImpl implements JpaUserDetailService{

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	private Logger logger = LoggerFactory.getLogger(JpaUserDetailServiceImpl.class);
		
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UsuarioEntity usuario = usuarioRepository.findByUsername(username);
		if(usuario == null) {
			logger.error("No existe");
			throw new UsernameNotFoundException(username + "No existe en el sistema.");
		}
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for(RoleEntity role : usuario.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
		}		
		if(authorities.isEmpty()) {
			logger.error("No tiene roles");
			throw new UsernameNotFoundException(username + "No tienes roles asignados.");
		}
		return new User(usuario.getUsername(), usuario.getPassword(), true, true, true, true, authorities);
	}

}
