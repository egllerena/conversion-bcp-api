package pe.com.example.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import pe.com.example.entity.MonedaEntity;
import pe.com.example.repository.MonedaRepository;
import pe.com.example.service.MonedaService;
import rx.Single;


@Service
public class MonedaServiceImpl implements MonedaService {
	
	@Autowired
	MonedaRepository monedaRepository;

	@Override
	public Single<List<MonedaEntity>> getAllMonedas(int limit, int page) {
		return buscarMonedasRepositorio(limit,page).map(this::ordenarMonedas);
	}
	
	private Single<List<MonedaEntity>> buscarMonedasRepositorio(int limit, int page) {
        return Single.create(singleSubscriber -> {
        	List<MonedaEntity> listaMonedas = new ArrayList<MonedaEntity>();
            if(!Objects.equals(limit, 0) && !Objects.equals(page, 0)) {
            	listaMonedas = monedaRepository.findAll(PageRequest.of(page, limit)).getContent();
            } else {
            	listaMonedas = monedaRepository.findAll();
            }            
            singleSubscriber.onSuccess(listaMonedas);
        });
    }

	private List<MonedaEntity> ordenarMonedas(List<MonedaEntity> monedasList) {
        return monedasList
                .stream()
                .sorted((x,y) -> x.getNombreMoneda().compareTo(y.getNombreMoneda()))
                .collect(Collectors.toList());
    }
}
