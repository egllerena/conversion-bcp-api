package pe.com.example.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.example.entity.UsuarioEntity;
import pe.com.example.repository.UsuarioRepository;
import pe.com.example.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService{
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public List<UsuarioEntity> obtenerTodosUsuarios(){
		return usuarioRepository.findAll();
	}
}