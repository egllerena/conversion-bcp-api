package pe.com.example.service;

import java.util.List;

import pe.com.example.entity.MonedaEntity;
import rx.Single;

public interface MonedaService {
    Single<List<MonedaEntity>> getAllMonedas(int limit, int page);
}
