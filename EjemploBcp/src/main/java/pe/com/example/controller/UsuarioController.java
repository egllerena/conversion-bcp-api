package pe.com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.example.dto.JwtResponse;
import pe.com.example.dto.UsuarioLogin;
import pe.com.example.entity.UsuarioEntity;
import pe.com.example.service.UsuarioService;

@CrossOrigin(origins="*",maxAge = 3600)
@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService; 
	
	@PostMapping("/login")
	public ResponseEntity<JwtResponse> login(@RequestBody UsuarioLogin usuario) {
		return ResponseEntity.ok(new JwtResponse());
	}
                		
	@GetMapping(value="/all",produces = "application/json")
	public List<UsuarioEntity> obtenerTodosUsuarios(){
		List<UsuarioEntity> lista = usuarioService.obtenerTodosUsuarios();
		return lista;
	}
}
