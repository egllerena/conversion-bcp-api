package pe.com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.com.example.dto.RespuestaBase;
import pe.com.example.entity.MonedaEntity;
import pe.com.example.service.MonedaService;
import rx.Single;
import rx.schedulers.Schedulers;

@CrossOrigin(origins="*",maxAge = 3600)
@RestController
@RequestMapping("/api/moneda")
public class MonedaController {
	@Autowired
	MonedaService monedaService;
	
	@GetMapping(value="/all/{limit}/{page}",produces = "application/json")
	public Single<ResponseEntity<RespuestaBase<List<MonedaEntity>>>> obtenerMonedas(
			@RequestParam(value = "limit", defaultValue = "0") int limit,
            @RequestParam(value = "page", defaultValue = "0") int page){
		return monedaService.getAllMonedas(0, 0)
				.subscribeOn(Schedulers.io())
				.map(monedaResponses -> ResponseEntity.ok(RespuestaBase.successWithData(monedaResponses)));
		
	}
}
