package pe.com.example.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.example.dto.ActualizarHistorialCambioDto;
import pe.com.example.dto.ConversionDto;
import pe.com.example.dto.ConversionStringDto;
import pe.com.example.dto.ConvertirDto;
import pe.com.example.dto.ConvertirStringDto;
import pe.com.example.dto.HistorialCambioFiltro;
import pe.com.example.dto.RespuestaBase;
import pe.com.example.entity.HistorialCambioEntity;
import pe.com.example.entity.MonedaEntity;
import pe.com.example.service.HistorialCambioService;
import rx.Single;
import rx.schedulers.Schedulers;

@CrossOrigin(origins="*",maxAge = 3600)
@RestController
@RequestMapping("/api/historialcambio")
public class HistorialCambioController {
	
	@Autowired
	HistorialCambioService historialCambioService;
	
	// Búsqueda por Ids
	@PostMapping(value = "/convertir", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Single<ResponseEntity<RespuestaBase<ConversionDto>>> convertirMontoRx(@RequestBody ConvertirDto convertirDto) throws IllegalAccessException {
		if(!Objects.equals(convertirDto.getMonedaBase(), null) && !Objects.equals(convertirDto.getMonedaCotizacion(), null)) {
			return historialCambioService.convertirMonto(convertirDto)
					.subscribeOn(Schedulers.io())
					.map(respuesta -> ResponseEntity.ok(RespuestaBase.successWithData(respuesta)));
		} else {
			throw new NullPointerException();
		}
	}
	
	// Búsqueda por nombres
	@PostMapping(value = "/convertirStringMoneda", consumes = MediaType.APPLICATION_JSON_VALUE, produces = "application/json;charset=UTF-8")
	public Single<ResponseEntity<RespuestaBase<ConversionStringDto>>> convertirMontoRxString(@RequestBody ConvertirStringDto convertirDto) throws IllegalAccessException {
		if(!Objects.equals(convertirDto.getMonedaBase(), null) && !Objects.equals(convertirDto.getMonedaCotizacion(), null)) {
			return historialCambioService.convertirMontoStringMoneda(convertirDto)
					.subscribeOn(Schedulers.io())
					.map(respuesta -> ResponseEntity.ok(RespuestaBase.successWithData(respuesta)));
		} else {
			throw new NullPointerException();
		}
	}
    	 
	@PostMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Single<ResponseEntity<RespuestaBase>> actualizarHistorialPost(@RequestBody HistorialCambioEntity historialCambioEntity) {
	   return historialCambioService.actualizarHistorial(historialCambioEntity)
	    		   .subscribeOn(Schedulers.io())
	    		   .toSingle(()->ResponseEntity.ok(RespuestaBase.successNoData()));
	}
	
	@PostMapping(value = "/actualizarPorNombreMoneda", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Single<ResponseEntity<RespuestaBase>> actualizarHistoriaPorlNombreMoneda(@RequestBody ActualizarHistorialCambioDto actualizarHistorialCambioDto) {
	   return historialCambioService.actualizarHistorialCambioPorNombreMoneda(actualizarHistorialCambioDto)
	    		   .subscribeOn(Schedulers.io())
	    		   .toSingle(()->ResponseEntity.ok(RespuestaBase.successNoData()));
	}
	
	@PostMapping(value="/allByFilter",produces = "application/json")
	public Single<ResponseEntity<RespuestaBase<List<HistorialCambioEntity>>>> obtenerHistoriales(@RequestBody HistorialCambioFiltro historialCambioFiltro){
		return historialCambioService.obtenerHistoriales(historialCambioFiltro)
				.subscribeOn(Schedulers.io())
				.map(historiales -> ResponseEntity.ok(RespuestaBase.successWithData(historiales)));			
	}
	
	@GetMapping(value="/obtenerHistorial/{idHistorial}")
	public Single<ResponseEntity<RespuestaBase<HistorialCambioEntity>>> obtenerHistorial(@PathVariable("idHistorial") Long idHistorial){
		return historialCambioService.obtenerHistorial(idHistorial)
				.subscribeOn(Schedulers.io())
				.map(historial -> ResponseEntity.ok(RespuestaBase.successWithData(historial)));
	}
	
	@PostMapping("/obtenerDetalleConversion")
	public Single<ResponseEntity<RespuestaBase<List<HistorialCambioEntity>>>> obtenerDetalleConversionWeb(@RequestBody List<MonedaEntity> listamonedas) {
		return historialCambioService.obtenerDetalleConversionWeb(listamonedas)
				.subscribeOn(Schedulers.io())
				.map(lista -> ResponseEntity.ok(RespuestaBase.successWithData(lista)));
	}
}
