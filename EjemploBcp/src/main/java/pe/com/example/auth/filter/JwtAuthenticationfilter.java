package pe.com.example.auth.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.example.auth.constantes.ConstantesAuth;
import pe.com.example.auth.service.JwtService;
import pe.com.example.entity.UsuarioEntity;

public class JwtAuthenticationfilter extends UsernamePasswordAuthenticationFilter {
	
	private AuthenticationManager authenticationManager;
	
	private JwtService jwtService;

	public JwtAuthenticationfilter(AuthenticationManager authenticationManager, JwtService jwtService) {
		this.authenticationManager = authenticationManager;
		setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/api/usuario/login","POST"));
		this.jwtService = jwtService;
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		String username = obtainUsername(request);
		String password = obtainPassword(request);		
		
		if(username != null && password != null) {
			logger.info("Username desde request parametr (form-data): "+username);
			logger.info("Username desde request parametr (form-data): "+password);
		} else {
			// Método para leer texto Json
			UsuarioEntity usuario = new UsuarioEntity();
			try {
				usuario = new ObjectMapper().readValue(request.getInputStream(), UsuarioEntity.class);
				username = usuario.getUsername();
				password = usuario.getPassword();
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
		username = username.trim();
		UsernamePasswordAuthenticationToken authtoken = new UsernamePasswordAuthenticationToken(username, password);
 		return authenticationManager.authenticate(authtoken);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
		String token = jwtService.crear(authResult);
		
		response.addHeader(ConstantesAuth.HEADER_AUTH, ConstantesAuth.TIPO_TOKEN +  token); //Generando Token Bearer
		
		// Guardando datos de respuesta
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("token", token);
		body.put("user", 
				new UsuarioEntity(null, ((User)authResult.getPrincipal()).getUsername(), null, jwtService.getRolesEntitys(((User)authResult.getPrincipal()).getAuthorities())));
		body.put("mensaje", String.format("Hola %s, has iniciado sesión", ((User)authResult.getPrincipal()).getUsername()));
		body.put("status", 200);
		
		// Enviando como json el map
		response.getWriter().write(new ObjectMapper().writeValueAsString(body));
		response.setStatus(200);
		response.setContentType("application/json");
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		// Guardando datos de respuesta
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("mensaje", String.format("Username o password incorrecto"));
		body.put("user", new UsuarioEntity());
		body.put("error", failed.getMessage());
		body.put("status", 403);
		
		// Enviando como json el map
		response.getWriter().write(new ObjectMapper().writeValueAsString(body));
		response.setStatus(403);
		response.setContentType("application/json");
	}
	

}
