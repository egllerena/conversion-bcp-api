package pe.com.example.auth.service;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import io.jsonwebtoken.Claims;
import pe.com.example.entity.RoleEntity;

public interface JwtService {
	public String crear(Authentication auth) throws IOException;
	public boolean validar(String token);
	public Claims getClaims(String token);
	public String getUsername(String token);
	public Collection<? extends GrantedAuthority> getRoles(String token) throws IOException; 
	public String resolverToken(String token);
	public List<RoleEntity> getRolesEntitys(Collection<? extends GrantedAuthority> roles);
}
