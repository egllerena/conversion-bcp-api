package pe.com.example.auth.filter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class MixinSimpleGrantedAuthority {
	
	// Señalando que atributo de authorities son los roles
	@JsonCreator
	public MixinSimpleGrantedAuthority(@JsonProperty("authority") String role) {
		
	}

}
