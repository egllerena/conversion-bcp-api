package pe.com.example.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import pe.com.example.auth.filter.JwtAuthenticationfilter;
import pe.com.example.auth.filter.JwtAuthorizationFilter;
import pe.com.example.auth.service.JwtService;
import pe.com.example.service.JpaUserDetailService;

@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
public class SpringSecurityConfig  extends WebSecurityConfigurerAdapter{	
		 
	@Autowired
	private JpaUserDetailService jpaUserDetailService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
		
	@Autowired
	private JwtService jwtService;
		
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
    	auth.userDetailsService(jpaUserDetailService).passwordEncoder(passwordEncoder);
    }

    
 //  endpoins con HTTP Basic authentication
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors().and().csrf().disable()        
        .authorizeRequests()
        .antMatchers("/").permitAll()
        .antMatchers("/h2-console/**").permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .addFilter(new JwtAuthenticationfilter(authenticationManagerBean(), jwtService))
        .addFilter(new JwtAuthorizationFilter(authenticationManagerBean(), jwtService))              
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and().headers().frameOptions().sameOrigin();    	
    }
    
    @Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
    
}
