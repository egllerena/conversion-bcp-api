package pe.com.example.auth.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import pe.com.example.auth.filter.MixinSimpleGrantedAuthority;
import pe.com.example.auth.service.JwtService;
import pe.com.example.entity.RoleEntity;
import pe.com.example.auth.constantes.ConstantesAuth;

@Service
public class JwtServiceImpl implements JwtService{

	@Override
	public String crear(Authentication auth) throws IOException {
		String username = ((User)auth.getPrincipal()).getUsername();
		Collection<? extends GrantedAuthority> roles = auth.getAuthorities();
		
		Claims claims = Jwts.claims();
		claims.put(ConstantesAuth.ATRIBUTO_ROLES, new ObjectMapper().writeValueAsString(roles));
		
		String token = Jwts.builder()
				.setClaims(claims)
				.setSubject(username)
				.signWith(SignatureAlgorithm.HS512, ConstantesAuth.CLAVE_SECRETA.getBytes())
				.setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + Long.valueOf(ConstantesAuth.TIEMPO_EXPIRACION))) //4horas expiración
				.compact();
		return token;
	}

	@Override
	public boolean validar(String token) {		
		// Manejar errores del token
		try {
			getClaims(token);
			return true;
		} catch (JwtException | IllegalArgumentException e) {
			return false;
		}
	}

	@Override
	public Claims getClaims(String token) {
		Claims claimsToken = Jwts.parser()
				.setSigningKey(ConstantesAuth.CLAVE_SECRETA.getBytes())
				.parseClaimsJws(token.replace(ConstantesAuth.TIPO_TOKEN, "")).getBody();
		return claimsToken;
	}

	@Override
	public String getUsername(String token) {
		return getClaims(token).getSubject();
	}

	@Override
	public Collection<? extends GrantedAuthority> getRoles(String token) throws IOException {
		Object roles = getClaims(token).get(ConstantesAuth.ATRIBUTO_ROLES);
		Collection<? extends GrantedAuthority> authorities = Arrays.asList(new ObjectMapper()
				.addMixIn(SimpleGrantedAuthority.class, MixinSimpleGrantedAuthority.class)
				.readValue(roles.toString().getBytes(), SimpleGrantedAuthority[].class));
		return authorities;
	}	

	@Override
	public String resolverToken(String token) {
		if(token != null && token.startsWith(ConstantesAuth.TIPO_TOKEN)) {
			return token.replace(ConstantesAuth.TIPO_TOKEN, "");
		}
		return null;
	}
	
	@Override
	public List<RoleEntity> getRolesEntitys(Collection<? extends GrantedAuthority> roles){
		List<RoleEntity> listaRoles = new ArrayList<>();
		for (GrantedAuthority grantedAuthority : roles) {
			listaRoles.add(new RoleEntity(null, grantedAuthority.getAuthority()));
		}		
		return listaRoles;
	}
}
