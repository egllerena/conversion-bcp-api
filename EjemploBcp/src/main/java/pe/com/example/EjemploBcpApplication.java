package pe.com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class EjemploBcpApplication extends SpringBootServletInitializer{
	
	public EjemploBcpApplication() {
		super();
		setRegisterErrorPageFilter(false);
	}	

	public static void main(String[] args) {
		SpringApplication.run(EjemploBcpApplication.class, args);
	}

}
